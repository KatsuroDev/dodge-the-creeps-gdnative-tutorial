# Setting up the project

In this first part, we'll organize the project's directory and figure what we need to get _GDNative_ set up.

## Project structure

Organising the project can be a little different with the addition of _GDNative_ scripts. So we'll follow Godot's example for the structure.

Launch Godot and create a new project.

![Creation of a project](res/gdnative_create_project.png)

Now create a parent directory to hold the project. Rename the project folder to `project`.

In this directory, create a `src` folder. It will hold all of our _C++_ source code.

![Project directory](res/gdnative_directory.png)

In the `project` folder, create a `gdnative` folder. Inside of it create 3 folders named  `linux`, `windows` and `macos`. We'll be needing those later.

## Compiling the bindings

We'll need a way to compile those source files. To do that, we'll use the python `Scons` from Godot's documentation and the Godot's _C++ bindings_. It's a build script that uses various compilers based on the platform you are on. Depending of your OS you might want to download additional dependencies.

Also download this [file](res/assets.zip), it contains all the assets we'll need and the `SConstruct` file.

To compile our source code, we'll need to link Godot's API. Godot gives out bindings to compile, so clone this [repository](https://github.com/godotengine/godot-cpp), with the following command.

`git clone --recursive -b 3.x https://github.com/godotengine/godot-cpp.git`

> Two good places to clone the repo is either next to your `src` folder. So you'll need bindings on every project. That's how Godot's example does it. Or, you can also clone the repo in the folder where you keep all your Godot projects. This way all the project share the same bindings. If you need different version e.g. `3.X` and `4`, just clone the repo a second time. Of course you can place it wherever you want.

So let's clone next to the `src` folder for simplicity. 

You could compile the bindings here directly with the repo's `SConstruct` file, but we'll compile it with the `SConstruct` from the file you downloaded.

Take the compressed assets folder, put the assets in the `project` folder and put the `SConstruct` next to the `src` folder.

![SConstruct directory](res/gdnative_scons_dir.png)

> If you cloned the repo somewhere else, you'll need to modify the path of the godot-cpp folder in the `SConstruct` file. Change `godot_bindings_path` to the path of your godot-cpp. If you renamed the repo folder, put the new name you called the folder. Let's say you named the repo folder `godot-cpp-3.x` change it to this `~/Documents/godot-cpp-3.x`.

![Binding paths](res/gdnative_binding_path.png)

Now run the command below where the `SConstruct` you downloaded is. Replace `<your_os>` with the name of the OS you are building for, in lower case.

`scons platform='<your_os>' -j8 rebuild_bindings=yes target=debug`

On linux it would be:

`scons platform=linux -j8 rebuild_bindings=yes target=debug`

> If you want to release your game, make sure to rebuild everything with the target set to `release`. It'll remove debug features, helping with performance.

There you go! Now the bindings are compiled.
The bindings are compiled to a library. This library is already linked to our `SConstruct` file, so it's ready to compile our code!

## Compiling your first script

Before we start the game's code, let's verify that the _GDNative_ scripts can run. Let's start with a simple **Hello, World!** printed to the console.

First file we'll create is the entry point. Godot will use this file to register any classes, methods, signals, etc. into it's database to let you use them inside the editor. 

`entry.cpp`
```c++
#include <Godot.hpp>

// Insert additional headers file here

using namespace godot;

extern "C" void GDN_EXPORT godot_gdnative_init(godot_gdnative_init_options *o) {
    Godot::gdnative_init(o);
}

extern "C" void GDN_EXPORT godot_gdnative_terminate(godot_gdnative_terminate_options *o) {
    Godot::gdnative_terminate(o);
}

extern "C" void GDN_EXPORT godot_nativescript_init(void *handle) {
    Godot::nativescript_init(handle);

    // Next lines are for registering classes.
    // register_class<Player>(); for example.
}
```

This is a big chunk, so we'll break it down.

---

```c++
extern "C" void GDN_EXPORT godot_gdnative_init(godot_gdnative_init_options *o) {
    Godot::gdnative_init(o);
}

extern "C" void GDN_EXPORT godot_gdnative_terminate(godot_gdnative_terminate_options *o) {
    Godot::gdnative_terminate(o);
}
```

These two functions are specifics for `GDNative`. They tell the `GDNative API` to init and terminate with certain options. Keep them like this, we won't touch them anymore. It's alright if you don't get it, understanding it is not needed to do GDNative, just know it is there.

---

```c++
extern "C" void GDN_EXPORT godot_nativescript_init(void *handle) {
    Godot::nativescript_init(handle);

    // Next lines are for registering classes.
    // register_class<Player>(); for example.
}
```

Here we first initialize the native script, we always need to do that, like the chunk on top. Next we'll need to tell our script what classes we have declared and want inside the editor. 
> Anything we register to the `API` is exposed in the editor, making it accessable from other scripts. However, everything else is obfuscated within the binaray of the library we are compiling.

---

Next, let's create our script header. Call it `hello.hpp`.

```c++
#ifndef HELLO_HPP
#define HELLO_HPP

#include <Godot.hpp>
#include <Node.hpp>

using namespace godot;

namespace godot
{
// Our hello world script is gonna be attached to a simple node.
class Hello : public Node {
    GODOT_CLASS(Hello, Node) // In first place put your class name, then the parent class name

public:
    void _ready(); // Same ready method as in GDScript
    static void _register_methods(); // Register methods to the GDNative API
}

}
```

For the constructor: 

```c++
class Hello : public Node {
    GODOT_CLASS(Hello, Node) // In first place put your class name, then the parent class name
```
This is pretty straight forward. It's like creating a new `gdscript` script. You choose your script's name (which is a class), then the parent node. Here same thing, your class' name inherits from the parent node. On the next line we tell the `GDNative API` about our new class. So `GODOT_CLASS(class_name, parent_name)`.

---

For the class' body :

```c++
public:
    void _ready(); // Same ready method as in GDScript
    static void _register_methods(); // Register methods to the GDNative API
```

Here we declare our methods and attributes. For now we only have methods. The `_register_methods()` is a static method (static is only declared on the class itself, not an instance/object) that is needed to register our methods to the `GDNative API`. The `_ready` method will be the equivalent of same named in `gdscript`. 

> Note that the `_ready` method could be call anything in the `c++` code. The `API` will associate it to the `_ready` callback when we register it in the `_register_methods` method. See below.

---

Now, for the source code, create a `hello.cpp` file.

```c++

#include "hello.hpp"

void Hello::_ready()
{
    Godot::print("Hello, World!");
}

void Hello::_register_methods()
{
    _register_method("_ready", &Hello::_ready);
}

```

> The following line of code: `_register_method("_ready", &Hello::_ready);` registers a method to the **GDNative API**. Since we want it to be the `_ready()` method, we need to write `_ready` as first parameter in a string. Then we reference the actual method. The actual method could be named anything else, we keep it the same to avoid confusion. You can create your own method that aren't registered in the **API**, they'd exist only in your _C++_, or you can register them so that then can be called from _Godot_.

This is it for the Hello script. Next, we'll need to update our `entry.cpp`.

```c++

#include <Godot.hpp>

// Insert additional headers file here
#include "hello.hpp" // We include our header

using namespace godot;

extern "C" void GDN_EXPORT godot_gdnative_init(godot_gdnative_init_options *o) {
    Godot::gdnative_init(o);
}

extern "C" void GDN_EXPORT godot_gdnative_terminate(godot_gdnative_terminate_options *o) {
    Godot::gdnative_terminate(o);
}

extern "C" void GDN_EXPORT godot_nativescript_init(void *handle) {
    Godot::nativescript_init(handle);

    // Next lines are for registering classes.
    // register_class<Player>(); for example.
    register_class<Hello>(); // We put our name class as the template
}

```

Add this line in your `entry.cpp` in the `godot_nativescript_init` method.
```c++
extern "C" void GDN_EXPORT godot_nativescript_init(void *handle) {
    Godot::nativescript_init(handle);
    register_class<Hello>(); // We put our name class as the template
}
```

---

Let's build all of that into binaries! So, we'll reuse the `SConstruct` file but with different parameters. We don't need to rebuild the bindings.

`scons platform='<your_os>' -j8 target=debug`

This will build a library for us to bind to the project. The library is built to `project/gdnative/your_os/libname`. You can change the directory in the `SConstruct` file at line 20.

|[Library's name and path](res/gdnative_lib_path.png)

`opts.Add(PathVariable("target_path", "The path where the lib is installed", "project/gdnative/"))`

## Bind script to the project

Now, we get to go back in the editor! We'll tell our project which lib to use.

Open the project in the editor and create a new resource. Select the `GDNativeLibrary` resource.

![Create library's resource](res/gdnative_libresource.png)

Rename it to `hello.gdnlib`. Save it in the `gdnative` folder.

Click on the file and in the bottom right corner click on the up pointing arrows.

![Up pointing arrows](res/gdnative_uparrows.png)

Look for your platform category. We're compiling in _64 bits_ for now so select the folder on the `64` line of your platform. Find the lib we compiled earlier, at `project/gdnative/<your_os>/library`. This file tells every script we'll attach to it that we want to use this specific library.

![Libraries menu](res/gdnative_lib_menu.png)

> If you are making a bigger project, you might want to separate into multiple libraries that each hold a specific mechanic.

Then create a new scene with a basic `Node` as root. Create a new script for it. Make it a `NativeScript` and put the class name exactly how we named the class in the _C++_ code. Save it in the `gdnative` folder.

![NativeScript creation](res/gdnative_createscript.png)

Select the root node, then it's script and look in the inspector. You should see the class name, you can update it anytime. The _Script Class_ menu let's you name your class and give it an icon if you are creating a custom node. We won't use that for this tutorial. Let's link our library to that script. Drag the `hello.gdnlib` into `Library`.

![Binding library to script](res/gdnative_inspectorbind.png)

Now for the moment of truth! Save the scene and run with `F5`.

`Hello, World!` should be printed to the console! If you have any errors, try to read through this page again and see if you missed anything. If everything's printing correctly, **congratulations** you just made your very first _GDNative_ script!
# Dodge The Creeps - GDNative Tutorial

This tutorial is a recreation of the tutorial game Dodge The Creeps from the Godot's documentation. It's aiming on the GDNative aspect to better help people with GDNative scripting. In this tutorial we'll use C++ and Godot 3.4.

## Description

You'll learn how to setup and structure a GDNative project, create a player controller script, create signals, connecting signals,
exporting variables, instancing scenes, etc. All this done in C++. We did this because we found that the Godot's documentation was lacking on the C++ aspect of scripting, so we decided to remake a simple project all in C++, to get all the basics.
